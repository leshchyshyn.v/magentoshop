<?php

namespace Ivanko\Hello\Controller\Index;

class NewAction extends \Magento\Framework\App\Action\Action
{
    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        echo "new";
        exit;
    }
}
