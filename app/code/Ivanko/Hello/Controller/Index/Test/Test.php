<?php

namespace Ivanko\Hello\Controller\Index\Test;

class Test extends \Magento\Framework\App\Action\Action
{
    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        echo "test from index";
        exit;
    }
}
