<?php

namespace Ivanko\A\Block;

/**
 * Class A
 * @package Ivanko\A\Block
 */
class A extends \Magento\Framework\View\Element\Template
{
    /**
     * @return \Magento\Framework\Phrase
     */
    public function getA()
    {
        return __("This is a block A");
    }
}