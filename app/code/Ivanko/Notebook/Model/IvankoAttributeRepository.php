<?php

namespace Ivanko\Notebook\Model;

use Ivanko\Notebook\Api\Data\IvankoAttributeInterface;
use Ivanko\Notebook\Api\IvankoAttributeRepositoryInterface;
use Ivanko\Notebook\Model\ResourceModel\IvankoAttribute as Resource;
use Ivanko\Notebook\Api\Data\IvankoAttributeInterfaceFactory;

class IvankoAttributeRepository implements IvankoAttributeRepositoryInterface
{
    /**
     * @var Resource
     */
    private $ivankoAttributeResource;

    /**
     * @var IvankoAttributeInterfaceFactory
     */
    private $attributeInterfaceFactory;

    /**
     * IvankoAttributeRepository constructor.
     * @param Resource $ivankoAttributeResource
     * @param IvankoAttributeInterfaceFactory $attributeInterfaceFactory
     */
    public function __construct(
        Resource $ivankoAttributeResource,
        IvankoAttributeInterfaceFactory $attributeInterfaceFactory
    )
    {
        $this->ivankoAttributeResource = $ivankoAttributeResource;
        $this->attributeInterfaceFactory = $attributeInterfaceFactory;
    }

    /**
     * @param integer $id
     * @return IvankoAttributeInterface
     */
    public function getById($id)
    {
        /** @var IvankoAttributeInterface $currIvankoAttribute */
        $currIvankoAttribute = $this->attributeInterfaceFactory->create();
        $this->ivankoAttributeResource->load($currIvankoAttribute, $id);
        return $currIvankoAttribute;
    }

    /**
     * @param IvankoAttributeInterface $ivankoAttribute
     * @return IvankoAttributeRepository
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
    public function save($ivankoAttribute)
    {
        $this->ivankoAttributeResource->save($ivankoAttribute);
        return $this;
    }

    /**
     * @param integer $orderId
     * @return IvankoAttributeInterface
     */
    public function getByOrderId($orderId)
    {
        /** @var IvankoAttributeInterface $currIvankoAttribute */
        $currIvankoAttribute = $this->attributeInterfaceFactory->create();
        $this->ivankoAttributeResource->load($currIvankoAttribute, $orderId, 'order_id');
        return $currIvankoAttribute;
    }
}