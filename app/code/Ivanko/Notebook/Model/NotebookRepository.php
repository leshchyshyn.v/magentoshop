<?php

namespace Ivanko\Notebook\Model;

use Ivanko\Notebook\Api\Data\NotebookInterface;
use Ivanko\Notebook\Api\Data\NotebookInterfaceFactory;
use Ivanko\Notebook\Api\NotebookRepositoryInterface;
use Ivanko\Notebook\Model\ResourceModel\Notebook as Resource;
use Magento\Framework\Api\SearchResultsInterfaceFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;

class NotebookRepository implements NotebookRepositoryInterface
{
    /**
     * @var NotebookInterfaceFactory
     */
    private $notebookFactory;

    /**
     * @var Resource
     */
    private $notebookResource;

    /**
     * @var Resource\CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var SearchResultsInterfaceFactory
     */
    private $searchFactory;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @var \Ivanko\Notebook\Model\ResourceModel\Notebook\Collection
     */
    private $collection;

    /**
     * NotebookRepository constructor.
     * @param NotebookInterfaceFactory $notebookFactory
     * @param Resource $notebookResource
     * @param Resource\CollectionFactory $collectionFactory
     * @param SearchResultsInterfaceFactory $searchFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        NotebookInterfaceFactory $notebookFactory,
        Resource $notebookResource,
        Resource\CollectionFactory $collectionFactory,
        SearchResultsInterfaceFactory $searchFactory,
        CollectionProcessorInterface $collectionProcessor
    )
    {
        $this->notebookFactory = $notebookFactory;
        $this->notebookResource = $notebookResource;
        $this->collectionFactory = $collectionFactory;
        $this->searchFactory = $searchFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @param integer $id
     * @return NotebookInterface
     */
    public function getById($id)
    {
        /** @var NotebookInterface $currNotebook */
        $currNotebook = $this->notebookFactory->create();
        $this->notebookResource->load($currNotebook, $id);
        return $currNotebook;
    }

    /**
     * @param NotebookInterface $notebook
     * @return NotebookRepositoryInterface
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
    public function save($notebook)
    {
        $this->notebookResource->save($notebook);
        return $this;
    }

    /**
     * @param NotebookInterface $notebook
     * @return void
     * @throws \Exception
     */
    public function delete($notebook)
    {
        $this->notebookResource->delete($notebook);
    }

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Magento\Framework\Api\SearchResultsInterface
     */
    public function getList($searchCriteria)
    {
        $this->collection = $this->collectionFactory->create();
        $this->collectionProcessor->process($searchCriteria, $this->collection);
        /** @var \Magento\Framework\Api\SearchResultsInterface $searchResult */
        $searchResult = $this->searchFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $searchResult->setItems($this->collection->getItems());
        $searchResult->setTotalCount($this->collection->getSize());
        return $searchResult;
    }

    /**
     * @return int
     */
    public function getLastPageNumber()
    {
        return $this->collection->getLastPageNumber();
    }
}