<?php

namespace Ivanko\Notebook\Model\ResourceModel;

use Ivanko\Notebook\Api\Data\IvankoAttributeInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class IvankoAttribute extends AbstractDb
{
    const TABLE_NAME = 'ivanko_attribute';

    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, IvankoAttributeInterface::ID);
    }
}