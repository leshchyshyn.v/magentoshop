<?php

namespace Ivanko\Notebook\Model;

use Ivanko\Notebook\Api\Data\IvankoAttributeInterface;
use Magento\Framework\Model\AbstractModel;
use Ivanko\Notebook\Model\ResourceModel\IvankoAttribute as Resource;

class IvankoAttribute extends AbstractModel implements IvankoAttributeInterface
{
    protected function _construct()
    {
        $this->_init(Resource::class);
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * @param integer $id
     * @return IvankoAttributeInterface|void
     */
    public function setId($id)
    {
        $this->seData(self::ID);
        return $this;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->getData(self::VALUE);
    }

    /**
     * @param string $value
     * @return IvankoAttributeInterface
     */
    public function setValue($value)
    {
        $this->setData(self::VALUE, $value);
        return $this;
    }

    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->getData(self::ORDER_ID);
    }

    /**
     * @param $orderId
     * @return IvankoAttributeInterface|void
     */
    public function setOrderId($orderId)
    {
        $this->setData(self::ORDER_ID, $orderId);
    }
}