<?php

namespace Ivanko\Notebook\Model;

use Ivanko\Notebook\Api\Data\NotebookInterface;
use Magento\Framework\Model\AbstractModel;
use Ivanko\Notebook\Model\ResourceModel\Notebook as Resource;

class Notebook extends AbstractModel implements NotebookInterface
{
    protected function _construct()
    {
        $this->_init(Resource::class);
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->getData('id');
    }

    /**
     * @param integer $id
     * @return NotebookInterface
     */
    public function setId($id)
    {
        $this->setData('id', $id);
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->getData('name');
    }

    /**
     * @param string $name
     * @return NotebookInterface
     */
    public function setName($name)
    {
        $this->setData('name', $name);
        return $this;
    }

    /**
     * @return string
     */
    public function getProcessor()
    {
        return $this->getData('processor');
    }

    /**
     * @param string $processor
     * @return NotebookInterface
     */
    public function setProcessor($processor)
    {
        $this->setData('processor', $processor);
        return $this;
    }

    /**
     * @return integer
     */
    public function getRam()
    {
        return $this->getData('ram');
    }

    /**
     * @param integer $ram
     * @return NotebookInterface
     */
    public function setRam($ram)
    {
        $this->setData('ram', $ram);
        return $this;
    }

    /**
     * @return integer
     */
    public function getVideoMemory()
    {
        return $this->getData('video_memory');
    }

    /**
     * @param integer $video_memory
     * @return NotebookInterface
     */
    public function setVideoMemory($video_memory)
    {
        $this->setData('video_memory', $video_memory);
        return $this;
    }

    /**
     * @return integer
     */
    public function getStatus()
    {
        return $this->getData('status');
    }

    /**
     * @param int $status
     * @return NotebookInterface
     */
    public function setStatus($status)
    {
        $this->setData('status', $status);
        return $this;
    }
}