<?php

namespace Ivanko\Notebook\Controller\Adminhtml\Index;

use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Ivanko\Notebook\Model\ResourceModel\Notebook\CollectionFactory;
use Ivanko\Notebook\Api\NotebookRepositoryInterface;

class MassDelete extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = "Ivanko_Notebook::base";

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $pageFactory;

    /**
     * @var Filter
     */
    private $filter;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var NotebookRepositoryInterface
     */
    private $notebookRepository;

    /**
     * Constructor
     *
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param NotebookRepositoryInterface $notebookRepository
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        NotebookRepositoryInterface $notebookRepository
    )
    {
        parent::__construct($context);
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->notebookRepository = $notebookRepository;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        foreach ($collection as $notebook) {
            $this->notebookRepository->delete($notebook);
        }
        $this->messageManager->addSuccess(__('A total of %1 record(s) have been deleted.', $collection->getSize()));
        $this->_redirect("notebooks/index/index");
    }
}
