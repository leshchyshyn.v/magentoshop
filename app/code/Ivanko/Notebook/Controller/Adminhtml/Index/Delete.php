<?php

namespace Ivanko\Notebook\Controller\Adminhtml\Index;

use Magento\Backend\App\Action\Context;
use Ivanko\Notebook\Api\NotebookRepositoryInterface;

class Delete extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = "Ivanko_Notebook::base";

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $pageFactory;

    /**
     * @var NotebookRepositoryInterface
     */
    private $notebookRepository;

    /**
     * Constructor
     *
     * @param Context $context
     * @param NotebookRepositoryInterface $notebookRepository
     */
    public function __construct(
        Context $context,
        NotebookRepositoryInterface $notebookRepository
    ) {
        parent::__construct($context);
        $this->notebookRepository = $notebookRepository;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        if ($id) {
            $notebook = $this->notebookRepository->getById($id);
            try {
                $this->notebookRepository->delete($notebook);
                $this->messageManager->addSuccess(__("The notebook was deleted success."));
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__("Couldn't deleted the notebook."));
            }
        } else {
            $this->messageManager->addErrorMessage(__("Can't find the notebook."));
        }
        $this->_redirect("notebooks/index/index");
    }
}
