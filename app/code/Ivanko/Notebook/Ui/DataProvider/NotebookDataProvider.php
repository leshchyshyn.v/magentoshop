<?php

namespace Ivanko\Notebook\Ui\DataProvider;

use Magento\Ui\DataProvider\AbstractDataProvider;
use Magento\Framework\App\Request\DataPersistorInterface;
use Ivanko\Notebook\Model\Notebook as NotebookModel;
use Ivanko\Notebook\Model\ResourceModel\Notebook\Collection;
use Ivanko\Notebook\Model\ResourceModel\Notebook\CollectionFactory;

class NotebookDataProvider extends AbstractDataProvider
{
    /**
     * @var Collection
     */
    protected $collection;

    /**
     * @var array
     */
    private $loadedData = [];

    /**
     * @var DataPersistorInterface
     */
    private $dataPersistor;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        DataPersistorInterface $dataPersistor,
        array $meta = [],
        array $data = []
    )
    {
        $this->collection = $collectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * @inheritdoc
     */
    public function getData(): array
    {
        if (!empty($this->loadedData)) {
            return $this->loadedData;
        }

        $items = $this->collection->getItems();

        /** @var NotebookModel $notebook */
        foreach ($items as $notebook) {
            $this->loadedData[$notebook->getId()] = $notebook->getData();
        }

        $data = $this->dataPersistor->get('notebook');
        if (!empty($data)) {
            $notebook = $this->collection->getNewEmptyItem();
            $notebook->setData($data);
            $this->loadedData[$notebook->getId()] = $notebook->getData();
            $this->dataPersistor->clear('notebook');
        }

        return $this->loadedData;
    }
}