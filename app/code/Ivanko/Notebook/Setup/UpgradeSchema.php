<?php

namespace Ivanko\Notebook\Setup;

use Ivanko\Notebook\Api\Data\NotebookInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * Upgrades DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        $connection = $installer->getConnection();
        if (version_compare($context->getVersion(), '1.0.1', '<')) {

            if ($connection->isTableExists("notebook")) {
                $connection->addColumn(
                    "notebook",
                    NotebookInterface::STATUS,
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        'nullable' => false,
                        'comment' => "Status"
                    ]
                );
            }
            $installer->endSetup();
        }
        $setup->endSetup();
    }
}