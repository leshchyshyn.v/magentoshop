<?php

namespace Ivanko\Notebook\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

interface IvankoAttributeInterface extends ExtensibleDataInterface
{
    const ID = 'id';
    const VALUE = 'value';
    const ORDER_ID = 'order_id';

    /**
     * @return integer
     */
    public function getId();

    /**
     * @param integer $id
     * @return IvankoAttributeInterface
     */
    public function setId($id);

    /**
     * @return string
     */
    public function getValue();

    /**
     * @param string $value
     * @return IvankoAttributeInterface
     */
    public function setValue($value);

    /**
     * @return integer
     */
    public function getOrderId();

    /**
     * @param $orderId
     * @return IvankoAttributeInterface
     */
    public function setOrderId($orderId);
}