<?php

namespace Ivanko\Horde\Block;

use Magento\Framework\View\Element\Template;

/**
 * Class HelloWorld
 * @package Ivanko\Hello\Block
 */
class ForTheHorde extends Template
{
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    private $date;

    /**
     * ForTheHorde constructor.
     * @param Template\Context $context
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     */
    public function __construct(
        Template\Context $context,
        \Magento\Framework\Stdlib\DateTime\DateTime $date
    ) {
        parent::__construct($context);
        $this->date = $date;
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getBattleRoar()
    {
        return __("For the Horde!");
    }

    /**
     * @return string
     */
    public function getCurrentDate()
    {
        return $this->date->gmtDate();
    }
}
