<?php

namespace Ivanko\B\Block;

/**
 * Class B
 * @package Ivanko\B\Block
 */
class B extends \Magento\Framework\View\Element\Template
{
    /**
     * @return \Magento\Framework\Phrase
     */
    public function getB()
    {
        return __("This is a block B");
    }
}