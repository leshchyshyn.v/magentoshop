<?php

namespace Ivanko\Smartphone\Controller\Adminhtml\Action;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class View extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = "Ivanko_Smartphone::read";

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $pageFactory;

    /**
     * Constructor
     *
     * @param Context $context
     * @param PageFactory $pageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $pageFactory
    ) {
        parent::__construct($context);
        $this->pageFactory = $pageFactory;
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->pageFactory->create();
        $resultPage->setActiveMenu("Ivanko_Smartphone::smartphone");
        $resultPage->getConfig()->getTitle()->set(__("Smartphones"));
        $resultPage->getConfig()->getTitle()->prepend(__("Smartphones"));
        return $resultPage;
    }
}
