<?php

namespace Ivanko\Smartphone\Controller\Adminhtml\Action;

use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Ivanko\Smartphone\Model\ResourceModel\Smartphone\CollectionFactory;
use Ivanko\Smartphone\Api\SmartphoneRepositoryInterface;

class MassDelete extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = "Ivanko_Smartphone::write";

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $pageFactory;

    /**
     * @var Filter
     */
    private $filter;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var SmartphoneRepositoryInterface
     */
    private $smartphoneRepository;

    /**
     * Constructor
     *
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param SmartphoneRepositoryInterface $smartphoneRepository
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        SmartphoneRepositoryInterface $smartphoneRepository
    )
    {
        parent::__construct($context);
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->smartphoneRepository = $smartphoneRepository;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        foreach ($collection as $smartphone) {
            $this->smartphoneRepository->delete($smartphone);
        }
        $this->messageManager->addSuccess(__('A total of %1 record(s) have been deleted.', $collection->getSize()));
        $this->_redirect("smartphone/action/view");
    }
}
