<?php

namespace Ivanko\Smartphone\Block;

use Ivanko\Smartphone\Api\Data\SmartphoneInterface;
use Ivanko\Smartphone\Api\SmartphoneRepositoryInterface;
use Ivanko\Smartphone\Model\ResourceModel\Smartphone as Resource;
use Magento\Framework\View\Element\Template;
use Ivanko\Smartphone\Model\ResourceModel\Smartphone\Collection;
use Ivanko\Smartphone\Model\ResourceModel\Smartphone\CollectionFactory;

class Load extends Template
{
    /**
     * @var SmartphoneRepositoryInterface
     */
    private $smartphoneRepository;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var Collection|null
     */
    private $collection = null;

    /**
     * Load constructor.
     * @param Template\Context $context
     * @param SmartphoneRepositoryInterface $smartphoneRepository
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        Template\Context $context,
        SmartphoneRepositoryInterface $smartphoneRepository,
        CollectionFactory $collectionFactory
    )
    {
        parent::__construct($context);
        $this->smartphoneRepository = $smartphoneRepository;
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * @return SmartphoneInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getSmartphone()
    {
        /** @var SmartphoneInterface $smartphone */
        $smartphone = $this->smartphoneRepository->getById($this->getRequest()->getParam('id'));
        return $smartphone;
    }

    /**
     * @return Resource\Collection|array
     */
    public function getSmartphoneCollection()
    {
        if (null === $this->collection) {
            /** @var Collection $collection */
            $this->collection = $this->collectionFactory->create();
        }
        return $this->collection;
    }

    /**
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        /** @var \Magento\Theme\Block\Html\Pager $pager */
        $pager = $this->getLayout()->createBlock(
            'Magento\Theme\Block\Html\Pager',
            'test.news.pager'
        );
        $pager
            ->setAvailableLimit(array(4 => 4, 8 => 8, 12 => 12))
            ->setShowPerPage(true)
            ->setCollection($this->getSmartphoneCollection()
            );
        $this->setChild('pager', $pager);
        return $this;
    }

    /**
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
}