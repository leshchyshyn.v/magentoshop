<?php

namespace Ivanko\Smartphone\Model\ResourceModel;

use Ivanko\Smartphone\Api\Data\SmartphoneInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Smartphone extends AbstractDb
{
    const TABLE_NAME = 'smartphone';

    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, SmartphoneInterface::ID);
    }
}