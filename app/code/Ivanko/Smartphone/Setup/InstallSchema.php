<?php

namespace Ivanko\Smartphone\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Ivanko\Smartphone\Api\Data\SmartphoneInterface;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     * @throws \Zend_Db_Exception
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        $table = $installer->getConnection()->newTable(
            $installer->getTable("smartphone")
        )->addColumn(
            SmartphoneInterface::ID,
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'ID'
        )->addColumn(
            SmartphoneInterface::MODEL,
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Model'
        )->addColumn(
            SmartphoneInterface::DISPLAY,
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Display'
        )->addColumn(
            SmartphoneInterface::RESOLUTION,
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Resolution'
        )->addColumn(
            SmartphoneInterface::PROCESSOR,
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Processor'
        )->addColumn(
            SmartphoneInterface::OS,
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Operating system'
        )->addColumn(
            SmartphoneInterface::STORAGE,
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => false, 'unsigned' => true],
            'Storage'
        )->addColumn(
            SmartphoneInterface::RAM,
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => false, 'unsigned' => true],
            'RAM'
        )->addColumn(
            SmartphoneInterface::CAMERA,
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Camera'
        )->addColumn(
            SmartphoneInterface::BATTERY,
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => false, 'unsigned' => true],
            'Battery'
        )->addColumn(
            SmartphoneInterface::PRICE,
            \Magento\Framework\DB\Ddl\Table::TYPE_FLOAT,
            null,
            ['nullable' => false, 'unsigned' => true],
            'Price'
        )->addColumn(
            SmartphoneInterface::DESCRIPTION,
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            1023,
            ['nullable' => false],
            'Description'
        )->addColumn(
            SmartphoneInterface::STATUS,
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => false, 'unsigned' => true],
            'Status'
        )->setComment(
            'The smartphone'
        );

        $installer->getConnection()->createTable($table);
        $installer->endSetup();
    }
}
