<?php

namespace Ivanko\Review\Controller\Index;

use Ivanko\Review\Api\Data\ReviewInterface;
use Ivanko\Review\Api\Data\ReviewInterfaceFactory;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Ivanko\Review\Api\ReviewRepositoryInterface;

class Index extends Action
{
    /**
     * @var PageFactory
     */
    private $pageFactory;

    /**
     * @var ReviewRepositoryInterface
     */
    private $repositoryInterface;

    /**
     * Index constructor.
     * @param Context $context
     * @param PageFactory $pageFactory
     * @param ReviewRepositoryInterface $repositoryInterface
     */
    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        ReviewRepositoryInterface $repositoryInterface
    )
    {
        parent::__construct($context);
        $this->pageFactory = $pageFactory;
        $this->repositoryInterface = $repositoryInterface;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        /** @var ReviewInterface $review */
        $review = $this->repositoryInterface->getById($this->getRequest()->getParam('id'));

        return $this->pageFactory->create();
    }
}
