<?php

namespace Ivanko\Review\Model;

use Ivanko\Review\Api\Data\ReviewInterface;
use Ivanko\Review\Model\ResourceModel\Review as Resource;
use Magento\Framework\Model\AbstractModel;

class Review extends AbstractModel implements ReviewInterface
{
    public function _construct()
    {
        $this->_init(Resource::class);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * @param int $id
     * @return ReviewInterface
     */
    public function setId($id)
    {
        $this->setData(self::ID, $id);
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->getData(self::TITLE);
    }

    /**
     * @param string $title
     * @return ReviewInterface
     */
    public function setTitle($title)
    {
        $this->setData(self::TITLE, $title);
        return $this;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->getData(self::CONTENT);
    }

    /**
     * @param string $content
     * @return ReviewInterface
     */
    public function setContent($content)
    {
        $this->setData(self::CONTENT, $content);
        return $this;
    }

    /**
     * @return string
     */
    public function getUserName()
    {
        return$this->getData(self::USER_NAME);
    }

    /**
     * @param $user_name
     * @return ReviewInterface
     */
    public function setUserName($user_name)
    {
        $this->setData(self::USER_NAME, $user_name);
        return $this;
    }
}
