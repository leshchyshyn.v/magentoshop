<?php

namespace Ivanko\Review\Api;

interface ReviewRepositoryInterface
{
    /**
     * @param integer $id
     * @return \Ivanko\Review\Api\Data\ReviewInterface
     */
    public function getById($id);

    /**
     * @param \Ivanko\Review\Api\Data\ReviewInterface $review
     * @return \Ivanko\Review\Api\Data\ReviewInterface
     */
    public function save($review);

    /**
     * @param \Ivanko\Review\Api\Data\ReviewInterface $review
     * @return void
     */
    public function delete($review);
}