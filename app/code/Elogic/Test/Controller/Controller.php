<?php

namespace Elogic\Test\Controller;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Psr\Log\LoggerInterface;

abstract class Controller extends Action
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    public function __construct(
        LoggerInterface $logger,
        Context $context
    )
    {
        $this->logger = $logger;
        parent::__construct($context);
    }


    public function execute()
    {
        $this->logger->info("Controller is working");
        echo self::class;
        die;
    }
}
