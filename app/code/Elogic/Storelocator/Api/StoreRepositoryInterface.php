<?php

namespace Elogic\Storelocator\Api;

interface StoreRepositoryInterface
{
    /**
     * Save store.
     *
     * @param \Elogic\Storelocator\Api\Data\StoreInterface $store
     * @return \Elogic\Storelocator\Api\Data\StoreInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Elogic\Storelocator\Api\Data\StoreInterface $store);

    /**
     * Retrieve store.
     *
     * @param int $storeId
     * @return \Elogic\Storelocator\Api\Data\StoreInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($storeId);

    /**
     * Retrieve stores matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Elogic\Storelocator\Api\Data\StoreSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete page.
     *
     * @param \Elogic\Storelocator\Api\Data\StoreInterface $store
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Elogic\Storelocator\Api\Data\StoreInterface $store);

    /**
     * Delete store by ID.
     *
     * @param int $storeId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($storeId);
}