<?php

namespace Elogic\Storelocator\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface for cms page search results.
 * @api
 * @since 100.0.2
 */
interface StoreSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get pages list.
     *
     * @return \Elogic\Storelocator\Api\Data\StoreInterface[]
     */
    public function getItems();

    /**
     * Set pages list.
     *
     * @param \Elogic\Storelocator\Api\Data\StoreInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
