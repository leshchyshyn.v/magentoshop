<?php
namespace Elogic\Storelocator\Controller\Index;

use Elogic\Storelocator\Api\StoreRepositoryInterface;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\View\Result\PageFactory;

class Index extends Action
{
    /**
     * @var Store
     */
    private $store;
    
    /**
     * @var PageFactory
     */
    protected $pageFactory;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfigl;

    /**
     * Index constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param StoreRepositoryInterface $storelocator
     * @param PageFactory $pageFactory
     * @param Context $context
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        StoreRepositoryInterface $storelocator,
        PageFactory $pageFactory,
        Context $context
    ) {
        $this->scopeConfigl = $scopeConfig;
        $this->store = $storelocator;
        $this->pageFactory = $pageFactory;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute()
    {

//        $key = $this->scopeConfigl->getValue("storelocator/general/apikey");
//        var_dump($key);die;
//        $this->store->load(2)->delete();
//        $this->store->setTitle("aecond store")->setAddress("Ruska st. 242")->save();
//        echo $this->store->getId();
//        $stores = $this->store->getCollection()->addFieldToFilter("entity_id", array("eq"=>3));
//        foreach ($stores as $item)
//        {
//            echo $item->getTitle();
//            echo "<br />";
//        }
        $store = $this->store->getById(1);
        echo $store->getTitle();
        die;
//        return  $resultPage = $this->pageFactory->create();
    }
}