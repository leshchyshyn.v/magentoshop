<?php

namespace Elogic\Storelocator\Controller\Adminhtml\Store;

use Elogic\Storelocator\Api\Data\StoreInterfaceFactory;
use Elogic\Storelocator\Api\StoreRepositoryInterface;
use Magento\Backend\App\Action\Context;
use Elogic\Storelocator\Api\Data\StoreInterface;

class Save extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = "Elogic_Storelocator::all";

    /**
     * @var StoreInterfaceFactory
     */
    private $storeFactory;

    /**
     * @var StoreRepositoryInterface
     */
    private $storeRepository;

    /**
     * Constructor
     *
     * @param Context $context
     * @param StoreInterfaceFactory $storeFactory
     * @param StoreRepositoryInterface $storeRepository
     */
    public function __construct(
        Context $context,
        StoreInterfaceFactory $storeFactory,
        StoreRepositoryInterface $storeRepository
    ) {
        parent::__construct($context);
        $this->storeFactory = $storeFactory;
        $this->storeRepository = $storeRepository;
    }

    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        /** @var StoreInterface $store */
        $store = $this->storeFactory->create();
        if (isset($data['entity_id']))
            $store->setId($data['entity_id']);
        $store
            ->setTitle($data['title'])
            ->setAddress($data['address'])
            ->setSchedule($data['schedule']);
        $this->storeRepository->save($store);
        $this->messageManager->addSuccess(__("The store was saved success."));
        $this->_redirect("storelocator/store/index");
    }
}
