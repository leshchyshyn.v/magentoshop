<?php

namespace Elogic\Link\Plugin;

use Elogic\Link\Api\Data\ErpNumberInterfaceFactory;
use Elogic\Link\Model\ErpNumberRepository;
use Magento\Sales\Api\Data\OrderExtensionFactory;

class OrderGet
{
    /**
     * @var ErpNumberInterfaceFactory
     */
    private $erpNumberFactory;

    /**
     * @var ErpNumberRepository
     */
    private $erpNumberRepository;

    /**
     * @var OrderExtensionFactory
     */
    private $orderExtensionFactory;

    /**
     * OrderGet constructor.
     * @param ErpNumberInterfaceFactory $erpNumberFactory
     * @param ErpNumberRepository $erpNumberRepository
     * @param OrderExtensionFactory $orderExtensionFactory
     */
    public function __construct(
        ErpNumberInterfaceFactory $erpNumberFactory,
        ErpNumberRepository $erpNumberRepository,
        OrderExtensionFactory $orderExtensionFactory
    )
    {
        $this->erpNumberRepository = $erpNumberRepository;
        $this->erpNumberFactory = $erpNumberFactory;
        $this->orderExtensionFactory = $orderExtensionFactory;
    }

    public function afterGet(
        \Magento\Sales\Api\OrderRepositoryInterface $subject,
        \Magento\Sales\Api\Data\OrderInterface $resultOrder
    ) {
        $resultOrder = $this->getExtensionAttribute($resultOrder);

        return $resultOrder;
    }

    private function getExtensionAttribute(\Magento\Sales\Api\Data\OrderInterface $order)
    {

        try {
            $erpNumber = $this->erpNumberRepository->getByOrderId($order->getEntityId());
        } catch (NoSuchEntityException $e) {
            return $order;
        }

        $extensionAttributes = $order->getExtensionAttributes();
        $orderExtension = $extensionAttributes ? $extensionAttributes : $this->orderExtensionFactory->create();
        $orderExtension->setErpNumber($erpNumber);
        $order->setExtensionAttributes($orderExtension);

        return $order;
    }
}