<?php

namespace Elogic\Link\Plugin;

use Elogic\Link\Model\ErpNumberRepository;
use Magento\Framework\Exception\CouldNotSaveException;

class OrderSave
{
    /**
     * @var ErpNumberRepository
     */
    private $erpNumberRepository;

    /**
     * OrderSave constructor.
     * @param ErpNumberRepository $erpNumberRepository
     */
    public function __construct(
        ErpNumberRepository $erpNumberRepository
    )
    {
        $this->erpNumberRepository = $erpNumberRepository;
    }

    public function afterSave(
        \Magento\Sales\Api\OrderRepositoryInterface $subject,
        \Magento\Sales\Api\Data\OrderInterface $resultOrder
    )
    {
        $resultOrder = $this->saveExtensionAttribute($resultOrder);

        return $resultOrder;
    }

    private function saveExtensionAttribute(\Magento\Sales\Api\Data\OrderInterface $order)
    {
        $extensionAttributes = $order->getExtensionAttributes();
        if (
            null !== $extensionAttributes &&
            null !== $extensionAttributes->getErpNumber()
        ) {
            $erpNumberAttribute = $extensionAttributes->getErpNumber();
            try {
                // The actual implementation of the repository is omitted
                // but it is where you would save to the database (or any other persistent storage)
                $this->erpNumberRepository->save($erpNumberAttribute);
            } catch (\Exception $e) {
                throw new CouldNotSaveException(
                    __('Could not add attribute to order: "%1"', $e->getMessage()),
                    $e
                );
            }
        }
        return $order;
    }
}