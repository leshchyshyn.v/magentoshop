<?php

namespace Elogic\Link\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

interface ErpNumberInterface extends ExtensibleDataInterface
{
    const ID = 'id';
    const ERP_ID = 'erp_id';
    const ORDER_ID = 'order_id';

    /**
     * @return integer
     */
    public function getId();

    /**
     * @param integer $id
     * @return ErpNumberInterface
     */
    public function setId($id);

    /**
     * @return string
     */
    public function getErpId();

    /**
     * @param string $erpId
     * @return ErpNumberInterface
     */
    public function setErpId($erpId);

    /**
     * @return integer
     */
    public function getOrderId();

    /**
     * @param $orderId
     * @return ErpNumberInterface
     */
    public function setOrderId($orderId);
}