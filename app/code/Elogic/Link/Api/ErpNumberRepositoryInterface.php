<?php

namespace Elogic\Link\Api;

interface ErpNumberRepositoryInterface
{
    /**
     * @param integer $id
     * @return \Elogic\Link\Api\Data\ErpNumberInterface
     */
    public function getById($id);

    /**
     * @param \Elogic\Link\Api\Data\ErpNumberInterface $erpNumber
     * @return \Elogic\Link\Api\Data\ErpNumberInterface
     */
    public function save($erpNumber);

    /**
     * @param integer $orderId
     * @return \Elogic\Link\Api\Data\ErpNumberInterface
     */
    public function getByOrderId($orderId);
}