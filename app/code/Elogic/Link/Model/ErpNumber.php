<?php
namespace Elogic\Link\Model;

use Magento\Framework\Model\AbstractModel;
use Elogic\Link\Api\Data\ErpNumberInterface;

class ErpNumber extends AbstractModel implements ErpNumberInterface
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Elogic\Link\Model\ResourceModel\ErpNumber::class);
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->getData($this::ID);
    }

    /**
     * @param integer $id
     * @return ErpNumberInterface
     */
    public function setId($id)
    {
        return $this->setData($this::ID, $id);
    }

    /**
     * @return string
     */
    public function getErpId()
    {
        return $this->getData($this::ERP_ID);
    }

    /**
     * @param string $erpId
     * @return ErpNumberInterface
     */
    public function setErpId($erpId)
    {
        return $this->setData($this::ERP_ID, $erpId);
    }

    /**
     * @return integer
     */
    public function getOrderId()
    {
        return $this->getData($this::ORDER_ID);
    }

    /**
     * @param $orderId
     * @return ErpNumberInterface
     */
    public function setOrderId($orderId)
    {
        return $this->setData($this::ORDER_ID, $orderId);
    }
}