<?php

namespace Elogic\Link\Model;

use Elogic\Link\Api\ErpNumberRepositoryInterface;
use Elogic\Link\Api\Data\ErpNumberInterface;
use Elogic\Link\Model\ResourceModel\ErpNumber as Resource;
use Elogic\Link\Api\Data\ErpNumberInterfaceFactory;

class ErpNumberRepository implements ErpNumberRepositoryInterface
{
    /**
     * @var Resource
     */
    private $resource;

    /**
     * @var ErpNumberInterfaceFactory
     */
    private $erpNumberFactory;

    /**
     * ErpNumberRepository constructor.
     * @param Resource $resource
     * @param ErpNumberInterfaceFactory $erpNumberFactory
     */
    public function __construct(
        Resource $resource,
        ErpNumberInterfaceFactory $erpNumberFactory
    )
    {
        $this->resource = $resource;
        $this->erpNumberFactory = $erpNumberFactory;
    }

    /**
     * @param integer $id
     * @return \Elogic\Link\Api\Data\ErpNumberInterface
     */
    public function getById($id)
    {
        /** @var ErpNumberInterface $erpNumber */
        $erpNumber = $this->erpNumberFactory->create();
        $this->resource->load($erpNumber, $id);
        return $erpNumber;
    }

    /**
     * @param \Elogic\Link\Api\Data\ErpNumberInterface $erpNumber
     * @return \Elogic\Link\Api\Data\ErpNumberInterface
     */
    public function save($erpNumber)
    {
        $this->resource->save($erpNumber);
        return $this;
    }

    /**
     * @param integer $orderId
     * @return \Elogic\Link\Api\Data\ErpNumberInterface
     */
    public function getByOrderId($orderId){
        /** @var ErpNumberInterface $erpNumber */
        $erpNumber = $this->erpNumberFactory->create();
        $this->resource->load($erpNumber, $orderId, 'order_id');
        return $erpNumber;
    }
}