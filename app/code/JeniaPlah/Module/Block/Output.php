<?php

namespace JeniaPlah\Module\Block;

use Magento\Framework\View\Element\Template;

/**
 * Class Output
 * @package JeniaPlah\Module\Block
 */
class Output extends Template
{

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $date;

    /**
     * Output constructor.
     * @param Template\Context $context
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     */
    public function __construct(
        Template\Context $context,
        \Magento\Framework\Stdlib\DateTime\DateTime $date)
    {
        parent::__construct($context);
        $this->date = $date;
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getOutput()
    {
        return __('Output from module!');
    }

    /**
     * @return string
     */
    public function getDate()
    {
        return $this->date->gmtDate();
    }
}