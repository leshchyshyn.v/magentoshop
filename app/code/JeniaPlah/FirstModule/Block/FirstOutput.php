<?php

namespace JeniaPlah\FirstModule\Block;

use Magento\Framework\View\Element\Template;

/**
 * Class FirstOutput
 * @package JeniaPlah\FirstModule\Block
 */
class FirstOutput extends Template
{
    /**
     * @return \Magento\Framework\Phrase
     */
    public function getOutput()
    {
        return __('Output from first module!');
    }
}