<?php

namespace JeniaPlah\Demo\Controller\Index;

class Save extends \Magento\Framework\App\Action\Action
{
    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        echo 'Save';
    }
}